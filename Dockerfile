# Composer for plugin
FROM composer:1.8.6 AS composer

ARG PLUGIN_DIR

WORKDIR /opt/composer/
COPY ./plugins/$PLUGIN_DIR .
RUN composer install

# Use an sngular openresty openid base as a parent image
FROM docker-registry.sngular.com:18444/repository/wordpress-base:v0.0.7
USER root

ARG PLUGIN_DIR

# Copy configuration files
COPY conf/nginx.conf /usr/local/openresty/nginx/conf/nginx.conf
COPY conf/phpsngular.ini /etc/php/7.2/fpm/conf.d/20-phpsngular.ini

# Copy own sngular code, like plugins or themes.
WORKDIR /var/www/html/wp-content
COPY ./themes ./themes
COPY ./plugins ./plugins
COPY --from=composer /opt/composer/vendor/ ./plugins/$PLUGIN_DIR/vendor/
RUN mkdir uploads && mkdir upgrades

# Download and extract external plugins
WORKDIR /var/www/html/wp-content/plugins
RUN chmod 755 process-plugins.sh
RUN ./process-plugins.sh /var/www/html/wp-content/plugins
RUN rm process-plugins.sh

WORKDIR /var/www/html
RUN chown -R www-data:www-data .
RUN find . -type d -exec chmod 755 {} \;
RUN find . -type f -exec chmod 644 {} \;

USER www-data

EXPOSE 8081

# Minimal healthcheck
HEALTHCHECK --interval=5m --timeout=3s CMD curl --fail http://localhost:8081/ || exit 1

CMD ["/opt/sngular/entrypoint.sh"]
